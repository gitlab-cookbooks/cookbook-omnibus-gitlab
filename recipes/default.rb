#
# Cookbook:: cookbook-omnibus-gitlab
# Recipe:: default
# License:: MIT
#
# # Copyright:: 2019, GitLab Inc.

cf_origin_pull_enforced = node['omnibus-gitlab']['cloudflare']['origin_pull']['enforced']
cf_origin_pull_cert_path = '/etc/gitlab/ssl/cf-origin-pull.pem'

if cf_origin_pull_enforced
  node.default['omnibus-gitlab']['gitlab_rb']['nginx']['ssl_verify_client'] = 'on'
  node.default['omnibus-gitlab']['gitlab_rb']['nginx']['ssl_client_certificate'] = cf_origin_pull_cert_path
end

user_ratelimit_bypasses = node['omnibus-gitlab']['user_ratelimit_bypasses']

if user_ratelimit_bypasses
  node.default['omnibus-gitlab']['gitlab_rb']['gitlab-rails']['env']['GITLAB_THROTTLE_USER_ALLOWLIST'] =
    user_ratelimit_bypasses.keys.join(",")
end
#
# merge_secrets takes the passed string (or array of strings),
#   gets the secret configuration at that location,
#   and merges the secrets with the node attributes
#   returning a hash of normal and secret attributes.
#

attributes_with_secrets = merge_secrets('omnibus-gitlab')

omnibus_pkg = node['omnibus-gitlab']['package']
pkg_url =
  if omnibus_pkg['use_key']
    "#{omnibus_pkg['scheme_url']}://#{attributes_with_secrets['package']['key']}:@#{omnibus_pkg['base_url']}"
  else
    "#{omnibus_pkg['scheme_url']}://#{omnibus_pkg['base_url']}"
  end
pkg_repo = omnibus_pkg['repo']
sources_file_name = pkg_repo.sub('/', '_')

package 'curl'

case node['platform_family']
when 'debian'
  include_recipe 'apt::default'

  # Check for 'expired' signature in repository's public GPG keys
  bash 'check repo gpg key' do
    code <<-EOF
      KEYRING_FILE=$(grep -o -m 1 "signed-by=.*]" /etc/apt/sources.list.d/#{sources_file_name}.list | sed -e 's/signed-by\=//' | tr -d ']')
      EXPIRED_KEY_MESSAGE=$(test -f $KEYRING_FILE && gpg --show-keys $KEYRING_FILE | grep '^pub.*expired.*]$' | wc -l)

      if [ $EXPIRED_KEY_MESSAGE -ne 0 ];
      then
        rm /etc/apt/sources.list.d/#{sources_file_name}.list
      fi
    EOF

    only_if { File.exist?("/etc/apt/sources.list.d/#{sources_file_name}.list") }
  end

  execute "add #{pkg_url}/#{pkg_repo} apt repo" do
    command "curl #{pkg_url}/install/repositories/#{pkg_repo}/script.deb.sh | bash"
    creates "/etc/apt/sources.list.d/#{sources_file_name}.list"
  end
  apt_package omnibus_pkg['name'] do
    version omnibus_pkg['version']
    options '--force-yes'
    timeout omnibus_pkg['timeout']
    notifies :run, 'execute[apt-get update]', :before
    notifies :run, 'execute[gitlab-ctl reconfigure]'
    only_if { omnibus_pkg['enable'] || !node['packages'].keys.include?(omnibus_pkg['name']) }
  end
when 'rhel'
  execute "add #{pkg_url}/#{pkg_repo} yum repo" do
    command "curl #{pkg_url}/install/repositories/#{pkg_repo}/script.rpm.sh | bash"
    creates "/etc/yum.repos.d/#{sources_file_name}.repo"
  end

  yum_package omnibus_pkg['name'] do
    version omnibus_pkg['version']
    timeout omnibus_pkg['timeout']
    notifies :run, 'execute[gitlab-ctl reconfigure]'
    allow_downgrade true
    only_if { omnibus_pkg['enable'] || !node['packages'].keys.include?(omnibus_pkg['name']) }
  end
end

# Create /etc/gitlab and its contents
directory '/etc/gitlab'

if attributes_with_secrets['gitlab_rb']['letsencrypt']['enable'] == true
  all_nginx = %w[nginx registry-nginx mattermost-nginx pages-nginx]

  all_nginx.each do |app|
    attributes_with_secrets['gitlab_rb'][app].delete('ssl_certificate')
    attributes_with_secrets['gitlab_rb'][app].delete('ssl_certificate_key')
  end
end

# For our ClickHouse databases, we have read-write and read-only credentials that we use for the
# read-write and read-only consoles respectively. We can easily override a cleartext value set in
# Chef role attributes, but we can't do that with secrets because there is only one secret for all of
# the secrets used by Omnibus so this block below handles picking the right password to use based on the
# value of the "read_only" boolean.
clickhouse_databases = attributes_with_secrets.dig('gitlab_rb', 'gitlab-rails', 'clickhouse_databases')
if clickhouse_databases
  clickhouse_databases.each_value do |attributes|
    if attributes['read_only'] && attributes['password_ro']
      attributes['password'] = attributes['password_ro']
    end

    attributes.delete('password_ro')
    attributes.delete('read_only')
  end
end

# Fetch encrypted secrets and node attributes
gitlab_rb = attributes_with_secrets['gitlab_rb']

unless node['omnibus-gitlab']['populate_disabled_services']
  # If a service is explicitly disabled, then don't include any of its attributes in
  # gitlab.rb except for its ['enable'] = false.
  gitlab_rb.each_key do |key|
    if gitlab_rb[key]['enable'] == false
      gitlab_rb[key] = {}
      gitlab_rb[key]['enable'] = false
    end
  end
end

if attributes_with_secrets['gitlab_rb']['gitaly']['enable'] && node['omnibus-gitlab']['gitaly']['deduplicate_storage_paths']
  storage_name = attributes_with_secrets['gitlab_rb']['git_data_dirs'].find do |storage, conf|
    # An actual node would be assigned to default because it's mandatory by Rails, but
    # we don't want to match it eagerly; we want to match the entry with the node name if it's present.
    # If not, we assume "default" storage anyway.
    next if storage == 'default'

    conf['gitaly_address'].include?(node.name)
  end
  storage_name = storage_name&.first || 'default'
  storage_conf = attributes_with_secrets['gitlab_rb']['gitaly']['configuration']['storage'].select { |storage| storage['name'] == storage_name }
  attributes_with_secrets['gitlab_rb']['gitaly']['configuration']['storage'] = storage_conf
  if (daily_maintenance = attributes_with_secrets['gitlab_rb']['gitaly']['configuration']['daily_maintenance'])
    daily_maintenance['storages'] = [storage_name]
  end
end

template '/etc/gitlab/gitlab.rb' do
  mode '0600'
  variables(gitlab_rb: gitlab_rb)
  helper(:single_quote) { |value| value.nil? ? nil : "'#{value}'" }
  notifies :run, 'execute[gitlab-ctl reconfigure]'
  sensitive true
end

file '/etc/gitlab/skip-auto-reconfigure' do
  if node['omnibus-gitlab']['skip_auto_reconfigure']
    action :create
  else
    action :delete
  end
end

directory '/etc/gitlab/ssl' do
  owner 'root'
  group 'git'
  mode '0750'
end

# Fetch encrypted secrets and node attributes
ssl = attributes_with_secrets['ssl']
ssh = attributes_with_secrets['ssh']

file node['omnibus-gitlab']['gitlab_rb']['nginx']['ssl_certificate'] do
  content ssl['certificate']
  not_if { ssl['certificate'].empty? }
  notifies :run, 'bash[reload nginx configuration]'
end

file node['omnibus-gitlab']['gitlab_rb']['nginx']['ssl_certificate_key'] do
  content ssl['private_key']
  not_if { ssl['private_key'].empty? }
  mode '0600'
  notifies :run, 'bash[reload nginx configuration]'
end

file node['omnibus-gitlab']['gitlab_rb']['mattermost-nginx']['ssl_certificate'] do
  content ssl['mattermost_certificate']
  not_if { ssl['mattermost_certificate'].empty? }
  notifies :run, 'bash[reload nginx configuration]'
end

file node['omnibus-gitlab']['gitlab_rb']['mattermost-nginx']['ssl_certificate_key'] do
  content ssl['mattermost_private_key']
  not_if { ssl['mattermost_private_key'].empty? }
  mode '0600'
  notifies :run, 'bash[reload nginx configuration]'
end

file node['omnibus-gitlab']['gitlab_rb']['pages-nginx']['ssl_certificate'] do
  content ssl['pages_certificate']
  not_if { ssl['pages_certificate'].empty? }
  notifies :run, 'bash[reload nginx configuration]'
end

file node['omnibus-gitlab']['gitlab_rb']['pages-nginx']['ssl_certificate_key'] do
  content ssl['pages_private_key']
  not_if { ssl['pages_private_key'].empty? }
  mode '0600'
  notifies :run, 'bash[reload nginx configuration]'
end

file node['omnibus-gitlab']['gitlab_rb']['registry-nginx']['ssl_certificate'] do
  content ssl['registry_certificate']
  not_if { ssl['registry_certificate'].empty? }
  notifies :run, 'bash[reload nginx configuration]'
end

file node['omnibus-gitlab']['gitlab_rb']['registry-nginx']['ssl_certificate_key'] do
  content ssl['registry_private_key']
  not_if { ssl['registry_private_key'].empty? }
  mode '0600'
  notifies :run, 'bash[reload nginx configuration]'
end

file node['omnibus-gitlab']['gitlab_rb']['gitaly']['configuration']['tls']['certificate_path'] do
  content ssl['gitaly_certificate']
  not_if { ssl['gitaly_certificate'].empty? }
  group 'git'
  notifies :run, 'execute[gitlab-ctl reconfigure]'
end if node.exist?('omnibus-gitlab', 'gitlab_rb', 'gitaly', 'configuration', 'tls', 'certificate_path')

file node['omnibus-gitlab']['gitlab_rb']['gitaly']['configuration']['tls']['key_path'] do
  content ssl['gitaly_private_key']
  not_if { ssl['gitaly_private_key'].empty? }
  group 'git'
  mode '0640'
  notifies :run, 'execute[gitlab-ctl reconfigure]'
end if node.exist?('omnibus-gitlab', 'gitlab_rb', 'gitaly', 'configuration', 'tls', 'key_path')

file node['omnibus-gitlab']['gitlab_rb']['gitaly']['configuration']['git']['signing_key'] do
  content ssh['gitaly_signing_key']
  not_if { ssh['gitaly_signing_key'].empty? }
  group 'git'
  mode '0640'
  notifies :run, 'execute[gitlab-ctl reconfigure]'
end if node.exist?('omnibus-gitlab', 'gitlab_rb', 'gitaly', 'configuration', 'git', 'signing_key')

file node['omnibus-gitlab']['gitlab_rb']['praefect']['configuration']['tls']['certificate_path'] do
  content ssl['praefect_certificate']
  not_if { ssl['praefect_certificate'].empty? }
  group 'git'
  notifies :run, 'execute[gitlab-ctl reconfigure]'
end if node.exist?('omnibus-gitlab', 'gitlab_rb', 'praefect', 'configuration', 'tls', 'certificate_path')

file node['omnibus-gitlab']['gitlab_rb']['praefect']['configuration']['tls']['key_path'] do
  content ssl['praefect_private_key']
  not_if { ssl['praefect_private_key'].empty? }
  group 'git'
  mode '0640'
  notifies :run, 'execute[gitlab-ctl reconfigure]'
end if node.exist?('omnibus-gitlab', 'gitlab_rb', 'praefect', 'configuration', 'tls', 'key_path')

file node['omnibus-gitlab']['gitlab_rb']['praefect']['configuration']['database']['sslcert'] do
  content ssl['praefect_database_client_certificate']
  not_if { ssl['praefect_database_client_certificate'].empty? }
  group 'git'
  notifies :run, 'execute[gitlab-ctl reconfigure]'
end if node.exist?('omnibus-gitlab', 'gitlab_rb', 'praefect', 'configuration', 'database', 'sslcert')

file node['omnibus-gitlab']['gitlab_rb']['praefect']['configuration']['database']['sslkey'] do
  content ssl['praefect_database_client_key']
  not_if { ssl['praefect_database_client_key'].empty? }
  group 'git'
  mode '0640'
  notifies :run, 'execute[gitlab-ctl reconfigure]'
end if node.exist?('omnibus-gitlab', 'gitlab_rb', 'praefect', 'configuration', 'database', 'sslkey')

file node['omnibus-gitlab']['gitlab_rb']['praefect']['configuration']['database']['sslrootcert'] do
  content ssl['praefect_database_server_ca']
  not_if { ssl['praefect_database_server_ca'].empty? }
  group 'git'
  mode '0640'
  notifies :run, 'execute[gitlab-ctl reconfigure]'
end if node.exist?('omnibus-gitlab', 'gitlab_rb', 'praefect', 'configuration', 'database', 'sslrootcert')

# Create /etc/gitlab/trusted-certs and its contents
directory '/etc/gitlab/trusted-certs'

ssl['trusted_certs'].each do |file_name, file_content|
  file File.join('/', 'etc', 'gitlab', 'trusted-certs', file_name) do
    content file_content
    notifies :run, 'bash[reload nginx configuration]'
  end
end

cookbook_file cf_origin_pull_cert_path do
  source 'cf-origin-pull.pem'
  mode '0600'
  action :create
  notifies :run, 'bash[reload nginx configuration]'
  only_if { node['omnibus-gitlab']['cloudflare']['origin_pull']['enforced'] }
end

# Run gitlab-ctl reconfigure if /etc/gitlab/gitlab.rb changed
execute 'gitlab-ctl reconfigure' do
  action :nothing
  # Set CONFIG to an empty string since gitlab-pages will
  # uses it as a config override
  # https://gitlab.com/gitlab-com/gl-infra/delivery/issues/612
  environment 'CONFIG' => ''
  only_if { node['omnibus-gitlab']['run_reconfigure'] }
end

# Reload NGINX if the SSL certificate or key has changed
bash 'reload nginx configuration' do
  code <<-SHELL
  if gitlab-ctl status nginx ; then
    gitlab-ctl hup nginx
  fi
  SHELL
  action :nothing
end

# Drop a metric indicating whether package updates are enabled
file omnibus_pkg['prom_metric_file'] do
  content %(omnibus_package_install{enable="#{omnibus_pkg['enable']}"} 1.0\n)
  mode "0644"
  only_if { ::Dir.exist?(File.dirname(omnibus_pkg['prom_metric_file'])) }
end
