#
# Cookbook:: cookbook-omnibus-gitlab
# Recipe:: backup_cron_job
# License:: MIT
#
# Manage a gitlab backup cron job
#
# # Copyright:: 2019, GitLab Inc.

backup_cron_job = node['omnibus-gitlab']['backup_cron_job']
pre_command = backup_cron_job['pre_command']
options = ''

options << " SKIP=#{backup_cron_job['skip'].join(',')}" if backup_cron_job['skip'].any?

options << ' CRON=1' if backup_cron_job['silent']
options << " >#{backup_cron_job['log']} 2>&1" if backup_cron_job['log']

cron 'GitLab backup' do
  command "#{pre_command}/opt/gitlab/bin/gitlab-rake gitlab:backup:create #{options}"
  hour backup_cron_job['hour']
  minute backup_cron_job['minute']
  weekday backup_cron_job['weekday']
  user backup_cron_job['user']
  action backup_cron_job['enable'] ? :create : :delete
end
